#!/usr/bin/env python3

import sqlite3
import pickle
import readcfg
import datetime
import time

class TagsStore:
    db = 'example.db'
    configFile = 'sites.yaml'
    now = ''
    def __init__(self):
        self.now = time.time()
        self.sites = readcfg.sitesList(self.configFile)
        self.conn = sqlite3.connect(self.db)
        c = self.conn.cursor()
        c.execute('''CREATE TABLE if not exists requests
            (site_name text, url text, date text, data text)''')
        self.conn.commit()

    def getSiteTags(self, strSite):
        c = self.conn.cursor()
        site = (strSite, strSite)
        c.execute('SELECT * FROM requests WHERE site_name=? OR url=?', site)
        tags = c.fetchone()
        if tags:
            tags = list(tags)
            tags[3] = pickle.loads(tags[3])
        return tags

    def setSiteTags(self, strSite, strTag, strCount):
        # Get site url from short name
        site = self.sites.getSite(strSite)
        # Load if exist Tags object
        tags = self.getSiteTags(site)
        if tags:
            # Update tags
            tags.date = self.now
            tags[3][strTag] = strCount
            tags = tuple(tags)
        else:
            # Create new row
            tags = pickle.dumps({strTag: strCount})
            tags = (strSite, site, self.now, tags)
        c = self.conn.cursor()
        c.execute('INSERT INTO requests VALUES (?,?,?,?)', tags)
        self.conn.commit()

    def __exit__(self, type_, value, traceback):
        self.conn.close()

