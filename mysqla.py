#!/usr/bin/env python3

import os
import sys
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
import pickle
import readcfg
import datetime

Base = declarative_base()

class Tags(Base):
    __tablename__ = 'tags'
    id = Column(Integer, primary_key=True)
    site_name = Column(String(250))
    url = Column(String(250))
    date = Column(String(250))
    tags = Column(String(250))

engine = create_engine('sqlite:///tagcounter.db')

if not engine.dialect.has_table(engine, 'tags'):  # If table don't exist, Create.
    Base.metadata.create_all(engine)

class TagsStore:
    session = {}
    configFile = 'sites.yaml'
    now = ''
    sites ={}
    def __init__(self):
        engine = create_engine('sqlite:///tagcounter.db')
        DBSession = sessionmaker(bind=engine)
        self.session = DBSession()
        self.now = now.isoformat()
        self.sites = readcfg.sitesList(self.configFile)

    def getSiteTags(self, strSite):
        try:
            tags = self.session.query(Tags).filter(Tags.site_name == strSite).one()
            session.commit()
            tags.tags = pickle.loads(tags.tags)
            return tags
        except DoesNotExistException: # whatever error/exception it is on SQLA
            return false

    def setSiteTags(self, strSite, strTag, strCount):
        # Get site url from short name
        site = self.sites.getSite(strSite)
        # Load if exist Tags object
        tags = self.getSiteTags(site)
        if tags:
            # Update tags
            tags.date = self.now
            tags.tags[strTag] = strCount
        else:
            # Create new row
            tags = pickle.dumps({strTag: strCount})
            tags = Tags(site_name=strSite, url=site, date=self.now, tags=tags)
        try:
            self.session.add(tags)
            self.session.commit()
            return true
        except DoesNotExistException: # whatever error/exception it is on SQLA
            return false

    def __exit__(self, type_, value, traceback):
        self.session.close()

# Write test

