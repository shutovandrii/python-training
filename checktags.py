#!/usr/bin/env python3

from bs4 import BeautifulSoup
import urllib.request
import urllib.error
import logging

class siteTags:
    # Init data.
    soup = {}
    def __init__(self, strSite):
        self.setLogger()
        if strSite.startswith('http'):
            self.strSite = 'http://' + strSite
        else:
            self.strSite = strSite
        try:
            urlData = urllib.request.urlopen(self.strSite)
            self.soup = BeautifulSoup(urlData.read(), "html.parser")
        except urllib.error.URLError as eurl:
            self.logger.debug('fault url')
        except urllib.error.HTTPError as e:
           self.logger.debug('failed to get page')

    def getTags(self, strTagName):
        self.logger.debug(self.strSite)
        return len(self.soup.find_all(strTagName))

    def setLogger(self):
        # replace to - logging.config.fileConfig('logging.conf')
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.DEBUG)
        hdlr = logging.FileHandler('tagcounter.log')
        formatter = logging.Formatter('%(asctime)s - %(message)s')
        hdlr.setFormatter(formatter)
        self.logger.addHandler(hdlr)
