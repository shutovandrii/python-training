from setuptools import setup

setup(
    name='tagcounter',
    version='1.0',
    author='Andrii_Shutov',
    py_modules=['tagcounter'],
    description='Программа для подсчета количество html-тэгов на странице.',
    entry_points={'console_scripts': ['tagcounter = tagcounter:main']},
)
