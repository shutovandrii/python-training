#!/usr/bin/env python3

import yaml

class sitesList:
    # File object.
    yamlMatchData = {}
    strFileName = ''
    def __init__(self, strFileName):
        self.strFileName = strFileName
        with open(self.strFileName, 'r') as stream:
            try:
                self.yamlMatchData = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)

    def getSite(self, shortName):
        if shortName in self.yamlMatchData:
            return self.yamlMatchData[shortName]
        else:
            return false

    def getAll(self):
        return self.yamlMatchData

    def setSite(self, shortName, longName):
        self.yamlMatchData[shortName] = longName;
        with open(self.strFileName, 'w') as outfile:
            return yaml.dump(self.yamlMatchData, outfile, default_flow_style=False)

    def delSite(self, shortName):
        del self.yamlMatchData[shortName];
        with open(self.strFileName, 'w') as outfile:
            return yaml.dump(self.yamlMatchData, outfile, default_flow_style=False)
