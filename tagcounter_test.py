#!/usr/bin/env python3

import unittest
import checktags
import readcfg

class TestStringMethods(unittest.TestCase):
    
    # Test counting tags from example
    def test_count_tags(self):
        dom = checktags.siteTags('google.com')
        count = dom.getTags('div')
        self.assertEqual(count, 16)
    
    # Test not existing url
    def test_wrong_url(self):
        try:
            checktags.siteTags('google.dev')
        except urllib.error.URLError:
            self.fail("'google.dev' request raised urllib.error.URLError unexpectedly!")
        except urllib.error.HTTPError:
            self.fail("'google.dev' request raised urllib.error.HTTPError unexpectedly!")

    # Test config not empty
    def test_config_empty(self):
        sites = readcfg.sitesList('sites.yaml')
        site = sites.getAll()
        self.assertNotEqual(len(site), 0)

    # Test correct config read
    def test_config_read(self):
        sites = readcfg.sitesList('sites.yaml')
        site = sites.getSite('ggl')
        self.assertEqual(site, 'google.com')

    # Test insert new site config
    def test_config_insert(self):
        config = readcfg.sitesList('sites.yaml')
        newSite = 'test.example'
        sites = config.getAll()
        sites = [index for index, value in sites.items() if value == newSite]
        if len(sites) == 0:
            site = newSite.split('.')
            vowels = ('a', 'e', 'i', 'o', 'u')
            siteShort = ''.join([l for l in site[0] if l not in vowels]);
            config.setSite(siteShort, newSite)
        else:
            siteShort = sites[0]
        site = config.getSite(siteShort)
        self.assertEqual(site, newSite)
        config.delSite(siteShort)


if __name__ == '__main__':
    unittest.main()
