#!/usr/bin/env python3

import tkinter as tk
from tkinter.ttk import *
import readcfg
import checktags
import sqlite

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.sites = readcfg.sitesList('sites.yaml')
        self.pack()
        self.create_widgets()
        self.tag = 'div'
    
    def create_widgets(self):
        frame1 = tk.Frame(self, bg='white')
        frame2 = tk.Frame(self, bg='white')
        
        self.combo = Combobox(frame1, width=20)
        values = list()
        sites = self.sites.getAll()
        for index, value in enumerate(sites):
            values.append(sites[value])
        self.combo['values'] = tuple(values)
        self.combo.current(0) #set the selected item
        self.combo.grid(row=0, column=0, pady=5, ipadx=2)

        self.select = tk.Button(frame1, width=6)
        self.select["text"] = "Выбрать"
        self.select["command"] = self.selectSite
        self.select.grid(row=0, column=1, pady=5)

        self.siteName = Entry(frame1, width=30)
        self.siteName.grid(row=1, column=0, columnspan=2, pady=5)

        self.load = tk.Button(frame2)
        self.load["text"] = "Загрузить"
        self.load["command"] = self.countTags
        self.load.grid(row=2, column=0, pady=5)
        
        self.quit = tk.Button(frame2, text="Показать из базы", fg="red", command=self.getSaved)
        self.quit.grid(row=2, column=1, pady=5)

        self.lbl = Label(frame2, text="Выберите сайт для подсчета тагов")
        self.lbl.grid(row=3, column=0, columnspan=2)

        frame1.pack(ipady=10)
        frame2.pack()
            
    def selectSite(self):
        self.siteName.delete(0, "end")
        self.siteName.insert(0, self.combo.get())

    def countTags(self):
        dom = checktags.siteTags(self.siteName.get());
        count = dom.getTags(self.tag)
        # Create new site in config if needed
        sites = self.sites.getAll()
        sites = [index for index, value in sites.items() if value == self.siteName.get()]
        if len(sites) == 0:
            site = self.siteName.get().split('.')
            vowels = ('a', 'e', 'i', 'o', 'u')
            siteShort = ''.join([l for l in site[0] if l not in vowels]);
            self.sites.setSite(siteShort, self.siteName.get())
        else:
            siteShort = sites[0]
        self.lbl.configure(text='Кол-во тагов <' + self.tag + '> на сайте ' + self.siteName.get() + ': ' + str(count))
        store = mysql.TagsStore()
        store.setSiteTags(siteShort, self.tag, str(count))

    def getSaved(self):
        store = mysql.TagsStore()
        tags = store.getSiteTags(self.siteName.get())
        if tags:
            tags = list(tags)
            self.lbl.configure(text='Кол-во тагов <' + self.tag + '> на сайте ' + self.siteName.get() + ': ' + str(tags[3][self.tag]))
        else:
            self.lbl.configure(text='Cайт ' + self.siteName.get() + ' отсутствует в базе.')

class WindowStart:
    def __init__(self):
        root = tk.Tk()
        root.title("Tag counter app")
        root.geometry('360x200')
        app = Application(master=root)
        app.mainloop()
